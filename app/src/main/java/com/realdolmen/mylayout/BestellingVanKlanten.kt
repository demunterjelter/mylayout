package com.realdolmen.mylayout

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.AppOpsManagerCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import com.google.firebase.firestore.FirebaseFirestore
import com.realdolmen.mylayout.data.KlantListAdapter
import com.realdolmen.mylayout.model.Klant
import kotlinx.android.synthetic.main.activity_bestelling_van_klanten.*
import kotlinx.android.synthetic.main.klanten_view.*

class BestellingVanKlanten : AppCompatActivity() {

    val db = FirebaseFirestore.getInstance()
    val allOrders = arrayListOf<Klant>()
    var idOfDocument =""


    private var layoutManager: RecyclerView.LayoutManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bestelling_van_klanten)


//----------------------- de data uit database firestore hale
        db.collection("users")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {

                    idOfDocument = document.id

                    allOrders.add(document.toObject(Klant::class.java))
                }
                layoutManager = LinearLayoutManager(this)
                val adapter = KlantListAdapter(allOrders, this)
                recyclerView.layoutManager = layoutManager
                recyclerView.adapter = adapter


            }
            .addOnFailureListener { exception ->
                println("error getting documents")
            }

        //-----------------------------------------------------------------------

    }

    fun deleteOrder(view: View) {
        db.collection("users").document(idOfDocument)
            .delete()
            .addOnSuccessListener { println("DocumentSnapshot successfully deleted!") }
            .addOnFailureListener { println( "Error deleting document") }




    }


}
