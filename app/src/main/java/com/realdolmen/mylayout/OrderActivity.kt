package com.realdolmen.mylayout

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.SeekBar
import android.widget.TextView
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_order.*
import kotlinx.android.synthetic.main.klanten_view.*

class OrderActivity : AppCompatActivity() {

    val db = FirebaseFirestore.getInstance()

    lateinit var slider: SeekBar
    lateinit var value: TextView
    var progressAantal: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order)



        slider = sb_aantal
        slider.max = 10

        value = tv_resultSeekbar

        slider.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                value.text = progress.toString()
                progressAantal = progress.toString()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                value.text = "tracking started" + slider.progress
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                value.text = "you selected " + slider.progress
            }
        })

    }

    fun goToKlantenBestellingActivity(view: View) {
        val intent = Intent(this, BestellingVanKlanten::class.java)
        intent.action = Intent.ACTION_SEND
        intent.type = "text/plain";
        //intent.putExtra(Intent.EXTRA_TEXT, "Welcome to the second activity" );
        startActivity(intent)


        //alles van de firestore
        val userEnBestelling = HashMap<String, Any>()
        userEnBestelling["kind"] = sp_soort.selectedItem.toString()
        userEnBestelling["amount"] = progressAantal
        userEnBestelling["naam"] = ed_naam.text.toString()
        userEnBestelling["voornaam"] = ed_voornaam.text.toString()
        userEnBestelling["telnr"] = ed_telfnr.text.toString()

        // Add a new document with a generated ID
        db.collection("users")
            .add(userEnBestelling)
            .addOnSuccessListener { documentReference ->
                println("succes")
            }
            .addOnFailureListener { e ->
                println("failure")
            }


    }


}
