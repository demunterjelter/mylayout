package com.realdolmen.mylayout.data

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.realdolmen.mylayout.R
import com.realdolmen.mylayout.model.Klant

class KlantListAdapter(private val list: ArrayList<Klant>, private val context: Context) :
    RecyclerView.Adapter<KlantListAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindItem(klant: Klant) {
            var name: TextView = itemView.findViewById(R.id.lv_naam) as TextView
            var firstName: TextView = itemView.findViewById(R.id.lv_voornaam) as TextView
            var phoneNumber: TextView = itemView.findViewById(R.id.lv_telnr) as TextView
            var amount: TextView = itemView.findViewById(R.id.lv_aantal) as TextView
            var kind: TextView = itemView.findViewById(R.id.lv_soort) as TextView

            name.text = klant.naam
            firstName.text = klant.voornaam
            phoneNumber.text = klant.telnr
            amount.text = klant.amount
            kind.text = klant.kind


        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): KlantListAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.klanten_view, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: KlantListAdapter.ViewHolder, position: Int) {
        holder.bindItem(list[position])
    }

}